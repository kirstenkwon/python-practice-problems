# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    backwords = reversed(word)
    comparingword = "".join(backwords) #need to use this because when you use "reversed," it will give you "r,a,c,e,c,a,r"
    if word == comparingword:           #and becuase of it, you need to use "".join.
        return True                     #if i just want to directly reverse it with no commas, use [::-1]
    else:                                  # example below:
        return False
    return word

word = "racecar"

print(is_palindrome(word))


## ANOTHER WAY TO GO ABOUT THIS ##

def is_palindrome(word):
    backwords = word[::-1]
    if backwords == word:
        return True
    else:
        return False

word = "racecar"

print(is_palindrome(word))

# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None

    for i in values:
        max_value = max(values)
        return max_value

values = [1, 100, 2, 5000]

print(max_in_list(values))

# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    half_of_members_list = members_list * .5
    if attendees_list == half_of_members_list:
        return True
    else:
        return False


members_list = 200
attendees_list = 100
print(has_quorum(attendees_list, members_list))
